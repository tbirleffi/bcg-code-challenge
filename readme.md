# BCG Code Challenge - Application

---

Stack: PHP, MySQL, Symfony Framework. 
This application can rapidly provision a Symfony Framework app, per environment.

### Task:
For this project, we'd like you to create a simple web-based contact management tool. The application should store Users and Agencies, and will need to have a login/authentication scheme. Users can belong to exactly one Agency, and can edit the contact information of themselves and the other users in their Agency. They can view (but not edit) the contact info of other Agencies and Users, but only if they are logged in at the time. Non-logged-in users should only be able to see Agency details, not individual Users. Both agencies and users should have all the standard contact fields we all know and love - name, phone, email, address, etc. Add anything extra you feel like.

### Results:
1. Login as: u:root/p:root, for full admin access.
2. Login as: u:agent1/p:agent1, to meet some requirements.
3. Login as: u:agent2/p:agent2, to meet all requirements.

## Mac OS X: Local Development

1. Start with a Mac
2. Install VirtualBox v4.3.20-96996, or higher:
```
http://download.virtualbox.org/virtualbox/4.3.20/VirtualBox-4.3.20-96996-OSX.dmg
```
3. Install Docker and Boot2Docker, that comes with the install: 
```
https://docs.docker.com/installation/mac
$(boot2docker shellinit)
```
4. Get Docker Compose, NOTE: Always check here, https://github.com/docker/fig/releases, for the latest release:
```
curl -L https://github.com/docker/compose/releases/download/1.1.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```
5. CREATE A FOLDER HERE: /Users/Sites
6. THIS PROJECT MUST BE CHECKED OUT HERE: /Users/Sites/{project-folder-name}
7. Open up a terminal, and cd /Users/Sites/{project-folder-name}
8. Run: boot2docker init
9. Run: boot2docker up
11. NOTE: Don't know the ip, just type: boot2docker ip
12. Open: /Users/{user}/.bash_profile, and paste:
```
export DOCKER_HOST=tcp://$(boot2docker ip 2>/dev/null):2376
export DOCKER_CERT_PATH=/Users/{user}/.boot2docker/certs/boot2docker-vm
export DOCKER_TLS_VERIFY=1
```
13. Restart terminal, and cd /Users/Sites/{project-folder-name}
14. Run the profile fix: ./profile-fix.sh
15. Edit your local environment files. Note: .env files are ignored and not checked in to your repository.
16. Run: ./setup.sh local
17. Note: If you sometimes get this error:
```
Couldn't connect to Docker daemon - you might need to run `boot2docker up`.
```
18. Just run: ./setup.sh local again
19. Browse to: {generated_ip}, and you should see something!

### MySQL: Local Connection

1. MySQL Host: 127.0.0.1
2. Username: root
3. Password: root
4. Database: {database_name}
5. Port: 3306 || {port_per_environment}
6. SSH Host: {generated_ip}
7. SSH User: docker
8. SSH Password: tcuser
9. SSH Port: 22

### Composer Commands: Run As Needed

1. docker-compose run composer self-update
2. docker-compose run composer update

### Setup Scripts: Per Environment

1. ./setup local

### Teardown Scripts: Per Environment

1. ./teardown local

### Local Host File: Config
1. {generated_ip} local.{domain_name}.com


