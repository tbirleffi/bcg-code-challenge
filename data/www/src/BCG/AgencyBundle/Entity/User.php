<?php

namespace BCG\AgencyBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;

class User extends BaseUser
{
	protected $name;
	protected $phone;
	protected $address;

    public function __construct()
    {
        parent::__construct();
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }
    
    public function getPhone()
    {
        return $this->phone;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }
    
    public function getAddress()
    {
        return $this->address;
    }

    public function getGroupsAsCollection()
    {
        return $this->getGroups();
    }

    public function setGroupsAsCollection($group)
    {
        if( !$this->hasRole('ROLE_SUPER_ADMIN') && 
            $this->hasRole('ROLE_ADMIN') ) 
        {
            $userAgencies = $this->getGroups();
            if( count($userAgencies) == 0 ) {
                return $this->addGroup($group);
            } else {
                $this->removeGroup($userAgencies[0]);
                $this->addGroup($group);
            }
        }
    }
}