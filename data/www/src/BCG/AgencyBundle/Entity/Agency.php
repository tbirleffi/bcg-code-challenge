<?php

namespace BCG\AgencyBundle\Entity;

use FOS\UserBundle\Model\Group as BaseGroup;

class Agency extends BaseGroup
{
	protected $phone;
	protected $address;
	protected $website;
	protected $email;

    public function __construct()
    {
        parent::__construct($this->name);
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }
    
    public function getPhone()
    {
        return $this->phone;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }
    
    public function getAddress()
    {
        return $this->address;
    }

    public function setWebsite($website)
    {
        $this->website = $website;
    }
    
    public function getWebsite()
    {
        return $this->website;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }
    
    public function getEmail()
    {
        return $this->email;
    }

}