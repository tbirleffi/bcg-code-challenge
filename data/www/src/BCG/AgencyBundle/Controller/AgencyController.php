<?php

namespace BCG\AgencyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use BCG\AgencyBundle\Entity\Agency;
use BCG\AgencyBundle\Form\AgencyType;

/**
 * Agency controller.
 *
 */
class AgencyController extends Controller
{
    /**
     * Creates a new Agency entity.
     *
     */
    public function createAction(Request $request)
    {
        $isAdmin = $this->container->get('security.context')->isGranted('ROLE_ADMIN');
        $isRoot = $this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN');
        if(!$isAdmin) return $this->redirect('/sorry');
        if(!$isRoot) return $this->redirect('/');

        $entity = new Agency();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('agency_show', array('id' => $entity->getId())));
        }

        return $this->render('BCGAgencyBundle:Agency:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Agency entity.
     *
     * @param Agency $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Agency $entity)
    {
        $form = $this->createForm(new AgencyType(), $entity, array(
            'action' => $this->generateUrl('agency_create'),
            'method' => 'POST',
        ));

        $form->add('btn', 'submit', array(
            'label' => 'Create',
            'attr' => array('class' => 'btn btn-default')
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Agency entity.
     *
     */
    public function newAction()
    {
        $isAdmin = $this->container->get('security.context')->isGranted('ROLE_ADMIN');
        $isRoot = $this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN');
        if(!$isAdmin) return $this->redirect('/sorry');
        if(!$isRoot) return $this->redirect('/');

        $entity = new Agency();
        $form   = $this->createCreateForm($entity);

        return $this->render('BCGAgencyBundle:Agency:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Agency entity.
     *
     */
    public function showAction($id)
    {
        $isAdmin = $this->container->get('security.context')->isGranted('ROLE_ADMIN');
        if(!$isAdmin) return $this->redirect('/sorry');

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BCGAgencyBundle:Agency')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Agency entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BCGAgencyBundle:Agency:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Agency entity.
     *
     */
    public function editAction($id)
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        $isAdmin = $this->container->get('security.context')->isGranted('ROLE_ADMIN');
        $isRoot = $this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN');
        if(!$isAdmin) return $this->redirect('/sorry');

        if(!$isRoot)
        {
            $userAgency = $user->getGroups();
            $userAgency = $userAgency[0];
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BCGAgencyBundle:Agency')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Agency entity.');
        }

        if(!$isRoot && $userAgency->getId() != $entity->getId()) {
            return $this->redirect($this->generateUrl('agency_show', array('id' => $entity->getId())));
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BCGAgencyBundle:Agency:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Agency entity.
    *
    * @param Agency $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Agency $entity)
    {
        $form = $this->createForm(new AgencyType(), $entity, array(
            'action' => $this->generateUrl('agency_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('btn', 'submit', array(
            'label' => 'Update',
            'attr' => array('class' => 'btn btn-default')
        ));

        return $form;
    }
    /**
     * Edits an existing Agency entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $isAdmin = $this->container->get('security.context')->isGranted('ROLE_ADMIN');
        if(!$isAdmin) return $this->redirect('/sorry');

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BCGAgencyBundle:Agency')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Agency entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('agency_show', array('id' => $id)));
        }

        return $this->render('BCGAgencyBundle:Agency:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Agency entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $isAdmin = $this->container->get('security.context')->isGranted('ROLE_ADMIN');
        $isRoot = $this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN');
        if(!$isAdmin) return $this->redirect('/sorry');
        if(!$isRoot) return $this->redirect('/');

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BCGAgencyBundle:Agency')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Agency entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect('/');
    }

    /**
     * Creates a form to delete a Agency entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('agency_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array
                (
                    'label' => 'Delete', 
                    'attr' => array('class' => 'btn btn-default')
                ))
            ->getForm()
        ;
    }
}
