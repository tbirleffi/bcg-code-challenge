<?php

namespace BCG\AgencyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;

use BCG\AgencyBundle\Entity\User;
use BCG\AgencyBundle\Form\UserType;

/**
 * User controller.
 *
 */
class UserController extends Controller
{
    /**
     * Creates a new User entity.
     *
     */
    public function createAction(Request $request)
    {
        $isAdmin = $this->container->get('security.context')->isGranted('ROLE_ADMIN');
        if(!$isAdmin) return $this->redirect('/sorry');

        $entity = new User();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) 
        {
            $groupId = $this->get('request')->request->get('bcg_agencybundle_user')['groupsAsCollection'];
            $user = $form->getData();
            $userManager = $this->container->get('fos_user.user_manager');
            $entity->setPlainPassword( $user->getPlainPassword() );
            $userManager->updatePassword($entity);

            $g = $this->getGroupManager()->findGroupBy(array('id' => $groupId));
            $entity->addGroup($g);

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('user_show', array('id' => $entity->getId())));
        }

        return $this->render('BCGAgencyBundle:User:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(User $entity)
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        $isRoot = $this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN');

        if(!$isRoot)
        {
            $userAgency = $user->getGroups();
            $userAgency = $userAgency[0];
        }

        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('user_create'),
            'method' => 'POST',
        ));

        $form->add('plainPassword', 'repeated', array
        (
            'required' => true,
            'type' => 'password',
            'first_options' => array('label' => 'New Password:*', 'attr' => array('class' => 'form-control')),
            'second_options' => array('label' => 'New Password Confirmation:*', 'label_attr' => array('style' => 'padding-top: 15px;'), 'attr' => array('class' => 'form-control')),
            'invalid_message' => 'fos_user.password.mismatch',
            'constraints' => array
            (
                new Length(array('min' => 5, 'minMessage' => 'This value should have a minimum of 5 characters.')),
                new Regex
                ( 
                    array
                    (
                        'pattern' => '((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20})', 
                        'message' => "Must contain one digit from 0-9. One lowercase character. One uppercase character."
                    ) 
                )
            ),
        ));

        $form->add('groupsAsCollection', 'entity', array(
            'class' => 'BCGAgencyBundle:Agency',
            'property' => 'name',
            'required' => true,
            'multiple' => false,
            'label' => 'Groups:*',
            'label_attr' => array('style' => (( isset($userAgency) ) ? 'display:none;' : '')),
            'attr' => array('class' => 'form-control', 'style' => (( isset($userAgency) ) ? 'display:none;' : '')),
            'data' => (( isset($userAgency) ) ? $userAgency : null)
        ));

        $form->add('btn', 'submit', array(
            'label' => 'Create',
            'attr' => array('class' => 'btn btn-default')
        ));
        return $form;
    }

    /**
     * Displays a form to create a new User entity.
     *
     */
    public function newAction()
    {
        $isAdmin = $this->container->get('security.context')->isGranted('ROLE_ADMIN');
        if(!$isAdmin) return $this->redirect('/sorry');

        $entity = new User();
        $form   = $this->createCreateForm($entity);

        return $this->render('BCGAgencyBundle:User:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a User entity.
     *
     */
    public function showAction($id)
    {
        $isAdmin = $this->container->get('security.context')->isGranted('ROLE_ADMIN');
        $isRoot = $this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN');
        if(!$isAdmin) return $this->redirect('/sorry');

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BCGAgencyBundle:User')->find($id);

        if( !$isRoot && $entity->getRoles()[0] == 'ROLE_SUPER_ADMIN') return $this->redirect('/');

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BCGAgencyBundle:User:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     */
    public function editAction($id)
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        $isAdmin = $this->container->get('security.context')->isGranted('ROLE_ADMIN');
        $isRoot = $this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN');
        if(!$isAdmin) return $this->redirect('/sorry');

        if(!$isRoot)
        {
            $userAgency = $user->getGroups();
            $userAgency = $userAgency[0];
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BCGAgencyBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        if(!$isRoot)
        {
            $entityAgency = $entity->getGroups();
            if( count($entityAgency) > 0 ) {
                $entityAgency = $entityAgency[0];
                if($userAgency->getId() != $entityAgency->getId()) {
                    return $this->redirect($this->generateUrl('user_show', array('id' => $entity->getId())));
                }
            } else {
                return $this->redirect($this->generateUrl('user_show', array('id' => $entity->getId())));
            }         
        }

        $userAgencies = $entity->getGroups();
        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $editForm->add('plainPassword', 'repeated', array
        (
            'required' => false,
            'type' => 'password',
            'first_options' => array('label' => 'Password:', 'attr' => array('class' => 'form-control')),
            'second_options' => array('label' => 'Password Confirmation:', 'label_attr' => array('style' => 'padding-top: 15px;'), 'attr' => array('class' => 'form-control')),
            'invalid_message' => 'fos_user.password.mismatch',
            'constraints' => array
            (
                new Length(array('min' => 5, 'minMessage' => 'This value should have a minimum of 5 characters.')),
                new Regex
                ( 
                    array
                    (
                        'pattern' => '((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20})', 
                        'message' => "Must contain one digit from 0-9. One lowercase character. One uppercase character."
                    ) 
                )
            ),
        ));

        $editForm->add('groupsAsCollection', 'entity', array(
            'class' => 'BCGAgencyBundle:Agency',
            'property' => 'name',
            'required' => true,
            'multiple' => false,
            'label' => 'Groups:*',
            'attr' => array('class' => 'form-control'),
            'data' => $userAgencies[0]
        ));

        return $this->render('BCGAgencyBundle:User:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a User entity.
    *
    * @param User $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('user_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('btn', 'submit', array(
            'label' => 'Update',
            'attr' => array('class' => 'btn btn-default')
        ));

        return $form;
    }
    /**
     * Edits an existing User entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $isAdmin = $this->container->get('security.context')->isGranted('ROLE_ADMIN');
        if(!$isAdmin) return $this->redirect('/sorry');

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BCGAgencyBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->add('plainPassword', 'repeated', array
        (
            'required' => false,
            'type' => 'password',
            'first_options' => array('label' => 'Password:', 'attr' => array('class' => 'form-control')),
            'second_options' => array('label' => 'Password Confirmation:', 'label_attr' => array('style' => 'padding-top: 15px;'), 'attr' => array('class' => 'form-control')),
            'invalid_message' => 'fos_user.password.mismatch',
            'constraints' => array
            (
                new Length(array('min' => 5, 'minMessage' => 'This value should have a minimum of 5 characters.')),
                new Regex
                ( 
                    array
                    (
                        'pattern' => '((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,20})', 
                        'message' => "Must contain one digit from 0-9. One lowercase character. One uppercase character."
                    ) 
                )
            ),
        ));

        $editForm->add('groupsAsCollection', 'entity', array(
            'class' => 'BCGAgencyBundle:Agency',
            'property' => 'name',
            'required' => true,
            'multiple' => false,
            'label' => 'Groups:*',
            'attr' => array('class' => 'form-control'),
        ));

        $editForm->handleRequest($request);
        if ($editForm->isValid()) 
        {
            $user = $editForm->getData();
            $userAgencies = $user->getGroups();

            if( $user->getPlainPassword() && 
                strlen( $user->getPlainPassword() ) > 0 ) 
            {
                $userManager = $this->container->get('fos_user.user_manager');
                $entity->setPlainPassword( $user->getPlainPassword() );
                $userManager->updatePassword($entity);
            }

            $em->flush();
            return $this->redirect($this->generateUrl('user_show', array('id' => $id)));
        }

        return $this->render('BCGAgencyBundle:User:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a User entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $isAdmin = $this->container->get('security.context')->isGranted('ROLE_ADMIN');
        if(!$isAdmin) return $this->redirect('/sorry');

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BCGAgencyBundle:User')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect('/');
    }

    /**
     * Creates a form to delete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array
                (
                    'label' => 'Delete', 
                    'attr' => array('class' => 'btn btn-default')
                ))
            ->getForm()
        ;
    }

    protected function getGroupManager()
    {
        return $this->container->get('fos_user.group_manager');
    }
}
