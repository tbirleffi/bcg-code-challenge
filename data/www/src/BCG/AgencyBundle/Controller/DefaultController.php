<?php

namespace BCG\AgencyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        $isAdmin = $this->container->get('security.context')->isGranted('ROLE_ADMIN');
        $isRoot = $this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN');

    	$agencies = $this->getDoctrine()
        ->getRepository('BCGAgencyBundle:Agency')
        ->findAll();

        $collection = array();
        if($isAdmin) {
            foreach ($agencies as $agency) {
                $users = $this->_getAllUsersByAgentGroup( $agency->getId() );
                $collection[] = array(
                    'agency' => $agency,
                    'users' => $users,
                    'editable' => ( ( $isRoot ) ? true : $this->_isUsersEditable($user, $agency) )
                );
            }
        } else {
            $collection = $agencies;
        }

        return $this->render('BCGAgencyBundle:Public:agencies.html.twig', array(
            'collection'  => $collection,
            'isAdmin'   => $isAdmin,
            'isRoot'    => $isRoot
        ));
    }

    public function sorryAction()
    {
        return $this->render('BCGAgencyBundle:Public:sorry.html.twig');
    }

    public function viewAgencyAction($id)
    {
        $agency = $this->getDoctrine()
        ->getRepository('BCGAgencyBundle:Agency')
        ->find($id);

        return $this->render('BCGAgencyBundle:Public:view-agency.html.twig', array('agency' => $agency));
    }

    protected function _addBtn($formBuilder, $label = 'Update') 
    {
        $formBuilder->add('btn', 'submit', array(
            'label' => $label,
            'attr' => array('class' => 'btn btn-default')
        ));
    }

    protected function _getAllUsersByAgentGroup($groupId) 
    {
        $results = array();
        $conn = $this->get('database_connection');
        $q = $conn->prepare
        (
            "
              SELECT * FROM users_to_agencies uta
              INNER JOIN users u
              WHERE uta.group_id = '" . $groupId . "'
              AND u.id = uta.user_id
              ORDER BY u.username asc
            "
        );

        $q->execute();
        return $q->fetchAll();
    }

    protected function _isUsersEditable($user, $agency) 
    {
        $result = false;
        $userAgentName = null;
        $userAgencies = $user->getGroups();

        if( isset($userAgencies) && count($userAgencies) == 1 ) {
            $userAgentName = $userAgencies[0]->getName();
        }

        if( $userAgentName == $agency->getName() ) $result = true;
        return $result;
    }
}
