<?php

namespace BCG\AgencyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'required' => true,
                'label' => 'Name:*',
                'constraints' => array(
                   new NotBlank(),
                ),
                'attr' => array('class' => 'form-control'),
            ))
            ->add('email', 'email', array(
                'required' => true,
                'label' => 'Email:*',
                'constraints' => array(
                   new NotBlank(),
                ),
                'attr' => array('class' => 'form-control'),
            ))
            ->add('username', 'text', array(
                'required' => true,
                'label' => 'Username:*',
                'constraints' => array(
                   new NotBlank(),
                ),
                'attr' => array('class' => 'form-control'),
            ))
            ->add('phone', 'text', array(
                'required' => false,
                'label' => 'Phone:',
                'attr' => array('class' => 'form-control'),
            ))
            ->add('enabled', 'checkbox', array(
                'required' => false,
                'label' => 'Enabled:',
                'attr' => array('class' => 'form-control'),
            ))
            ->add('address', 'textarea', array(
                'required' => false,
                'label' => 'Address:',
                'attr' => array('class' => 'form-control', 'style' => 'height:100%;', 'rows' => 5),
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BCG\AgencyBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bcg_agencybundle_user';
    }
}
