<?php

namespace BCG\AgencyBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Yaml\Parser;

class Agencies extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    private $container;

    function getOrder()
    {
        return 1;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $obj)
    {
        // Load the agencies fixtures.
    	$this->loadFixtures
        (
            'agencies.yml', 
            'agencies',
            $this->getGroupManager()
        );
    }

    protected function loadFixtures($file, $name, $manager)
    {
        $data = $this->getFixture($file);
        $data = $data[$name];
        $this->buildFixtures($manager, $data);
    }

    protected function buildFixtures($manager, $data)
    {
        switch ($manager->getClass()) 
        {
            case 'BCG\AgencyBundle\Entity\Agency':
                if( sizeof($data) > 0 ) $this->buildAgencies($manager, $data);
                break;
        }
    }

    protected function buildAgencies($manager, $data)
    {
        foreach ($data as $value)
        {
            $sticky = ( isset($value['sticky']) ) ? $value['sticky'] : false;
            $n = ( isset($value['name']) ) ? $manager->findGroupByName($value['name']) : false; 

            if($n)
            {
                if($sticky == true)
                {
                    $group = $n;
                    $this->createOrUpdateAgency($manager, $value, $group);
                }
            }
            else $this->createOrUpdateAgency($manager, $value);
        }
    }

    protected function createOrUpdateAgency($manager, $data, $group = null)
    {
        if(!$group) $group = $manager->createGroup($data['name']);
        $group->setName($data['name']);
        $group->setRoles($data['permissions']);
        $group->setEmail($data['email']);
        $group->setWebsite( ( isset($data['website']) ) ? $data['website'] : null );
        $group->setAddress( ( isset($data['address']) ) ? $data['address'] : null );
        $group->setPhone( ( isset($data['phone']) ) ? $data['phone'] : null );
        $manager->updateGroup($group);
    }

    protected function getGroupManager()
    {
        return $this->container->get('fos_user.group_manager');
    }

    protected function getFixture($file)
    {
        $yaml = new Parser();
        return $yaml->parse(file_get_contents(__DIR__ . '/../../Resources/config/fixtures/' . $file));
    }
}