<?php

namespace BCG\AgencyBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Yaml\Parser;

class Users extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    private $container;

    function getOrder()
    {
        return 2;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $obj)
    {
        // Load the users fixtures.
        $this->loadFixtures
        (
            'users.yml', 
            'users',
            $this->getUserManager()
        );
    }

    protected function loadFixtures($file, $name, $manager)
    {
        $data = $this->getFixture($file);
        $data = $data[$name];
        $this->buildFixtures($manager, $data);
    }

    protected function buildFixtures($manager, $data)
    {
        switch ($manager->getClass()) 
        {
            case 'BCG\AgencyBundle\Entity\User':
                if( sizeof($data) > 0 ) $this->buildUsers($manager, $data);
                break;
        }
    }

    protected function buildUsers($manager, $data)
    {
        foreach ($data as $value)
        {
            $sticky = ( isset($value['sticky']) ) ? $value['sticky'] : false;
            $u = ( isset($value['username']) ) ? $manager->findUserByUsername($value['username']) : false;

            if($u)
            {
                if($sticky == true) $this->createOrUpdateUser($manager, $value, $u);
            }
            else $this->createOrUpdateUser($manager, $value);
        }
    }

    protected function createOrUpdateUser($manager, $v, $user = null)
    {
        if(!$user)
        {
            $user = $manager->createUser();
            $user->setEmail($v['email']);
        }

        $user->setUsername($v['username']);
        $user->setPlainPassword($v['password']);
        $user->setSuperAdmin( ( isset($v['super_admin']) ) ? $v['super_admin'] : false );
        $user->setEnabled( ( isset($v['enabled']) ) ? $v['enabled'] : true );
        $user->setExpired( ( isset($v['expired']) ) ? $v['expired'] : false );
        $user->setName( ( isset($v['name']) ) ? $v['name'] : null );
        $user->setPhone( ( isset($v['phone']) ) ? $v['phone'] : null );
        $user->setAddress( ( isset($v['address']) ) ? $v['address'] : null );

        if( count( $v['agencies'] ) > 0 ) 
        {
            foreach($v['agencies'] as $group)
            {
                $g = $this->getGroupManager()->findGroupByName($group);
                $user->addGroup($g);
            }
        }

        $manager->updateUser($user);
    }

    protected function getGroupManager()
    {
        return $this->container->get('fos_user.group_manager');
    }

    protected function getUserManager()
    {
        return $this->container->get('fos_user.user_manager');
    }

    protected function getFixture($file)
    {
        $yaml = new Parser();
        return $yaml->parse(file_get_contents(__DIR__ . '/../../Resources/config/fixtures/' . $file));
    }
}