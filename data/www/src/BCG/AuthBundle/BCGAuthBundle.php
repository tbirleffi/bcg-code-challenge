<?php

namespace BCG\AuthBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class BCGAuthBundle extends Bundle
{
	public function getParent()
    {
        return 'FOSUserBundle';
    }
}
