<?php

namespace BCG\AuthBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseSecurityController;

class SecurityController extends BaseSecurityController
{
    protected function renderLogin(array $data)
    {
        return $this->render('BCGAuthBundle:Security:login.html.twig', $data);
    }
}
